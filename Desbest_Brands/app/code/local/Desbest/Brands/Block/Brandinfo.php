<?php
class Desbest_Brands_Block_Brandinfo extends Mage_Core_Block_Template {

    public function _prepareLayout() {
        return parent::_prepareLayout();
    }

    public function brandInfo() {
        $brand = Mage::getModel('brands/brands')
                        ->load(Mage::app()
                                ->getRequest()
                                ->getParam('manufacturer'), 'attributelabelid');
        if ($brand->getBrandId()) {
            return $brand;
        } else {
       $attributeInfo = Mage::getResourceModel('eav/entity_attribute_collection')
                        ->setCodeFilter('manufacturer')->getFirstItem();
        $collection = Mage::getResourceModel('eav/entity_attribute_option_collection')
                        ->setPositionOrder('asc')
                        ->setAttributeFilter($attributeInfo->getAttributeId())
                        ->setStoreFilter(Mage::app()->getStore()->getId());
            $item = $collection->getItemByColumnValue("option_id", Mage::app()->getRequest()->getParam('manufacturer'));
            return $item;
        }
    }
    
    public function ExtraBrand(){
        $param = $this->getRequest()->getParam('manufacturer');
        $collection = Mage::getModel('brands/brands')->getCollection();
        $collection->addFieldToFilter('attributelabelid', $param);
        $extrabrand = $collection->getFirstItem();
        
        return $extrabrand;
    }

}