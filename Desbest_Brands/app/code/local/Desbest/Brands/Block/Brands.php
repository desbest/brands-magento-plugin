<?php
class Desbest_Brands_Block_Brands extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'brands';
    $this->_blockGroup = 'brands';
    $this->_headerText = Mage::helper('brands')->__('Manage Brands');
    $this->_addButtonLabel = Mage::helper('brands')->__('Add Brand');
    parent::__construct();
  }
}
