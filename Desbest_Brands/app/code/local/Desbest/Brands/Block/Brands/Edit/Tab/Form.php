<?php

class Desbest_Brands_Block_Brands_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm()
  {
      $form = new Varien_Data_Form();
      $this->setForm($form);
      $fieldset = $form->addFieldset('brands_form', array('legend'=>Mage::helper('brands')->__('Brand information')));
     
      $fieldset->addField('title', 'text', array(
          'label'     => Mage::helper('brands')->__('Brand name'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'title',
      ));

      $fieldset->addField('imagepath', 'image', array(
          'label'     => Mage::helper('brands')->__('Brand logo'),
          'required'  => false,
          'name'      => 'imagepath',
	  ));
	  
        $supplier_items = Mage::getModel('eav/entity_attribute_option')->getCollection()->setStoreFilter()->join('attribute','attribute.attribute_id=main_table.attribute_id', 'attribute_code');
        foreach ($supplier_items as $supplier_item) :
          if ($supplier_item->getAttributeCode() == 'manufacturer')
            $supplier_options[$supplier_item->getOptionId()] = $supplier_item->getValue();
        endforeach; //desbest edit

		
      $fieldset->addField('attributelabelid', 'select', array(
          'label'     => Mage::helper('brands')->__('Brand'),
          'name'      => 'attributelabelid',
            'type'      => 'options',
            'options'   => $supplier_options,
      ));
     
      $fieldset->addField('description', 'editor', array(
          'name'      => 'description',
          'label'     => Mage::helper('brands')->__('Description'),
          'title'     => Mage::helper('brands')->__('Description'),
          'style'     => 'width:700px; height:500px;',
          'wysiwyg'   => false,
          'required'  => false,
      ));
     
      if ( Mage::getSingleton('adminhtml/session')->getBrandsData() )
      {
          $form->setValues(Mage::getSingleton('adminhtml/session')->getBrandsData());
          Mage::getSingleton('adminhtml/session')->setBrandsData(null);
      } elseif ( Mage::registry('brands_data') ) {
          $form->setValues(Mage::registry('brands_data')->getData());
      }
      return parent::_prepareForm();
  }
}