<?php

class Desbest_Brands_Block_Brands_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
  public function __construct()
  {
      parent::__construct();
      $this->setId('brandsGrid');
      $this->setDefaultSort('id');
      $this->setDefaultDir('ASC');
      $this->setSaveParametersInSession(true);
  }

  protected function _prepareCollection()
  {
      $collection = Mage::getModel('brands/brands')->getCollection();
      $this->setCollection($collection);
      return parent::_prepareCollection();
  }

  protected function _prepareColumns()
  {
      $this->addColumn('id', array(
          'header'    => Mage::helper('brands')->__('ID'),
          'align'     =>'right',
          'width'     => '50px',
          'index'     => 'id',
      ));
      
        $thecode = "manufacturer";
        $attributeId = Mage::getResourceModel('eav/entity_attribute')->getIdByCode('catalog_product', $thecode);
        $attribute = Mage::getModel('catalog/resource_eav_attribute')->load($attributeId);
        $attributeOptions = $attribute ->getSource()->getAllOptions();
        
        $supplier_items = Mage::getModel('eav/entity_attribute_option')->getCollection()->setStoreFilter()->join('attribute','attribute.attribute_id=main_table.attribute_id', 'attribute_code');
        foreach ($supplier_items as $supplier_item) :
          if ($supplier_item->getAttributeCode() == 'manufacturer')
            $supplier_options[$supplier_item->getOptionId()] = $supplier_item->getValue();
        endforeach; //desbest edit
        
      $this->addColumn('title', array(
          'header'    => Mage::helper('brands')->__('Title'),
          'align'     =>'left',
          'width'     => '120px',
          'index'     => 'title',
            'type'      => 'options',
            'options'   => $supplier_options,
      ));
      
      $this->addColumn('description', array(
          'header'    => Mage::helper('brands')->__('Description'),
          'align'     =>'left',
          //'width'     =>'120px',
          'index'     => 'name',
      ));


      /*
        $this->addColumn('content', array(
        'header'    => Mage::helper('brands')->__('Item Content'),
        'width'     => '150px',
        'index'     => 'content',
        ));
      */
            
      $this->addColumn('status', array(
          'header'    => Mage::helper('brands')->__('Status'),
          'align'     => 'left',
          'width'     => '80px',
          'index'     => 'status',
          'type'      => 'options',
          'options'   => array(
              1 => 'Enabled',
              2 => 'Disabled',
          ),
      ));
	  
        $this->addColumn('action',
            array(
                'header'    =>  Mage::helper('brands')->__('Action'),
                'width'     => '100',
                'type'      => 'action',
                'getter'    => 'getId',
                'actions'   => array(
                    array(
                        'caption'   => Mage::helper('brands')->__('Edit'),
                        'url'       => array('base'=> '*/*/edit'),
                        'field'     => 'id'
                    )
                ),
                'filter'    => false,
                'sortable'  => false,
                'index'     => 'stores',
                'is_system' => true,
        ));
		
		$this->addExportType('*/*/exportCsv', Mage::helper('brands')->__('CSV'));
		$this->addExportType('*/*/exportXml', Mage::helper('brands')->__('XML'));
	  
      return parent::_prepareColumns();
  }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('id');
        $this->getMassactionBlock()->setFormFieldName('brands');

        $this->getMassactionBlock()->addItem('delete', array(
             'label'    => Mage::helper('brands')->__('Delete'),
             'url'      => $this->getUrl('*/*/massDelete'),
             'confirm'  => Mage::helper('brands')->__('Are you sure?')
        ));

        $statuses = Mage::getSingleton('brands/status')->getOptionArray();

        array_unshift($statuses, array('label'=>'', 'value'=>''));
        $this->getMassactionBlock()->addItem('status', array(
             'label'=> Mage::helper('brands')->__('Change status'),
             'url'  => $this->getUrl('*/*/massStatus', array('_current'=>true)),
             'additional' => array(
                    'visibility' => array(
                         'name' => 'status',
                         'type' => 'select',
                         'class' => 'required-entry',
                         'label' => Mage::helper('brands')->__('Status'),
                         'values' => $statuses
                     )
             )
        ));
        return $this;
    }

  public function getRowUrl($row)
  {
      return $this->getUrl('*/*/edit', array('id' => $row->getId()));
  }

}
