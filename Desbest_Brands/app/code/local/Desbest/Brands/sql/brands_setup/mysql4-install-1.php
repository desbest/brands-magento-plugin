<?php
//echo 'Running This Upgrade: '.get_class($this)."\n <br /> \n"; die("Exit for now");

$installer = $this;
$installer->startSetup();
$installer->run("
 
-- DROP TABLE IF EXISTS {$this->getTable('brands_brands')};
CREATE TABLE {$this->getTable('brands_brands')} (
  `id` int(11) unsigned NOT NULL auto_increment,
  `attributelabelid` varchar(100) NOT NULL,
  `title` varchar(255) NOT NULL default '',
  `description` varchar(255) NOT NULL,
  `imagepath` varchar(100) NOT NULL,
  
  `status` smallint(6) NOT NULL default '0',
  `created_time` datetime NULL,
  `update_time` datetime NULL,
  PRIMARY KEY (`id`)
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
 
-- INSERT INTO {$this->getTable('brands_brands')} (name, description, other) values ('Example 1', 'Example One Description', 'This first example is reall awesome.');
-- INSERT INTO {$this->getTable('brands_brands')} (name, description, other) values ('Example 2', 'Example Two Description', 'This second example is reall awesome.');
-- INSERT INTO {$this->getTable('brands_brands')} (name, description, other) values ('Example 3', 'Example Three Description', 'This third example is reall awesome.');
-- INSERT INTO {$this->getTable('brands_brands')} (name, description, other) values ('Example 4', 'Example Four Description', 'This fourth example is reall awesome.');
 
");
 
$installer->endSetup();