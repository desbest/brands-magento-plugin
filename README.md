# Brands Magento Plugin

This magento plugin allows you to assign brands to a product.

This plugin took 6 months to make.

## Credits

Copyright desbest 2012

## Features

* Filter by brand when browsing the shop
* The brand is shown on the product listing, for a user to click on to see more of the brand
* Products can be filtered by brand in the admin panel

## Compatibility

This was tested on Magento 1.7.0.2 (released in 2012). 

Magento 1.x is deprecated in favour of Magento 2, and that's not backwards compatible.

## Bugs

Uploading an image to assign to a brand doesn't work. A more skilled Magento developer needs to fix this.